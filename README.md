
# Starilex Nightstar

Nightstar is a plugin providing a suite of tools to extend the functionality of the official Kerbal 
Space Program Part Tools developed by Squad.

## Requirments

Nightstar is not a standalone software and requires a Unity project with KSP Part Tools installed.

As of the time of July 2022, Nightstar requires the following software which has been tested with
the reported versions 

- **Unity** *2019.4.18f1*
- **TextMesh Pro Release** *1.0.56* for Unity *2017.3*
- **KSP Part Tools** *2019.4.18f1*

The download for these files alongside some other instructions can be found at the Official PartTools
 thread on the Kerbal Forums.
**https://forum.kerbalspaceprogram.com/index.php?/topic/160487-official-parttools/**

## Installation

Once KSP Part Tools has been installed, open the Unity project folder in a file explorer
and copy the Starilex Nightstar folder inside the Plugins folder found at this path:

**ProjectPath\\Assets\\Plugins\\**

## Functionality

 - [x] Coordinate Recalculator
 
 - [x] Props Tools

 - [ ] Enhanced Props Spawner
 
 - [x] Textured IVAs on Editor 


## License
AGPL 3.0
